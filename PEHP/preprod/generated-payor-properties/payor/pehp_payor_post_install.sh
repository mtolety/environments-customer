#!/bin/bash

prefix=SL
suffixCount=3
idSeparator=-
payorUser=weblogic
payorHosts=172.19.29.214,172.19.29.215,172.19.29.216,172.19.29.217,172.19.29.219,172.19.29.222
weblogicDomainDir=/home/weblogic/oracle/wls_12.2.1.4.0/user_projects/domains/preprod
weblogicJarsDir=/home/weblogic/oracle/wls_12.2.1.4.0/user_projects/domains/preprod/jars
weblogicMappingFile=/home/weblogic/oracle/wls_12.2.1.4.0/user_projects/domains/preprod/data/defaultpathnamemapping.txt


export IFS=","
for payorHost in $payorHosts; do

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#PREFIX#|${prefix}|g' ${weblogicJarsDir}/SupplierLocationIDGeneratorConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#SUFFIX_COUNT#|${suffixCount}|g' ${weblogicJarsDir}/SupplierLocationIDGeneratorConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#ID_SEPARATOR#|${idSeparator}|g' ${weblogicJarsDir}/SupplierLocationIDGeneratorConfig.properties"

echo "Replacing defaultpathnamemapping on $payorHost"
ssh -q  ${payorUser}@${payorHost} "echo -n >  ${weblogicMappingFile}"
ssh -q  ${payorUser}@${payorHost} "cat > ${weblogicMappingFile} << EOF
COBPolicy=COB {0}
COBPolicy.caseStatus=COB Status {0}
EOF"

done

exit 0
