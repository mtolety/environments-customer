#!/bin/bash

prefix=
useYear4Digits=NA
useYearLastTwoDigits=TRUE
useJulianDate=NA
useMonthAndDay=TRUE
useBatchNumber=NA
prefixDelimiter=
batchDelimiter=
numberOfDigits=6
useClaimRecieptDate=NA
useClaimCurrentDate=TRUE
dailyResetStartingNumber=1
increment=1
payorUser=thc
payorHosts=172.23.14.162
weblogicDomainDir=/home/thc/oracle/wls_12.2.1.2.0/user_projects/domains/test
weblogicJarsDir=/home/thc/oracle/wls_12.2.1.2.0/user_projects/domains/test/jars
weblogicMappingFile=/home/thc/oracle/wls_12.2.1.2.0/user_projects/domains/test/data/defaultpathnamemapping.txt


export IFS=","
for payorHost in $payorHosts; do

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#PREFIX#|${prefix}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#USEYEAR_4DIGITS#|${useYear4Digits}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#USEYEAR_LAST_DIGITS#|${useYearLastTwoDigits}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#USE_JULIAN_DATE#|${useJulianDate}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#USE_MONTH_DAY#|${useMonthAndDay}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#USE_BATCH_NUMBER#|${useBatchNumber}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#PREFIX_DELIMITER#|${prefixDelimiter}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#BATCH_DELIMITER#|${batchDelimiter}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#NO_OF_DIGITS#|${numberOfDigits}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#USE_CLAIM_RECIEPT_DATE#|${useClaimRecieptDate}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#USE_CLAIM_CURRENT_DATE#|${useClaimCurrentDate}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#DAILY_STARTING_NUMBER#|${dailyResetStartingNumber}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

ssh -q  ${payorUser}@${payorHost} "sed -i -e 's|#INCREMENT#|${increment}|g' ${weblogicJarsDir}/ClaimIDGenConfig.properties"

echo "Replacing defaultpathnamemapping on $payorHost"
ssh -q  ${payorUser}@${payorHost} "echo -n >  ${weblogicMappingFile}"
ssh -q  ${payorUser}@${payorHost} "cat > ${weblogicMappingFile} << EOF
COBPolicy=COB {0}
COBPolicy.caseStatus=COB Status {0}
Membership.individual.taxIdentificationNumber=SSN
Membership.individual.genderCode=Gender
Membership.individual.birthDate=DOB
Membership.individual.primaryName.middleName=MiddleName
Membership.individual.primaryName.firstName=FirstName
Membership.individual.primaryName.lastName=LastName
EOF"

done

exit 0
